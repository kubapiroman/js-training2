/* Zadanie ze stringami*/




// const text1 = 'powiększ mnie!'
// const text2 = 'ZAPISZ MNIE MAŁYMI LITERAMI'
// const text3 = '!@#$% wytnij te dziwne znaki z początku'
// const text4 = 'sprawdź, czy zawieram słowo "czy"'
// const text5 = 'wyLoguj w konsoli tylko literę "L", która znajduje się w wyrazie "wyloguj"'
// const text6 = 'pies zamień każde słowo pies, na słowo kot pies'
// const text7 = 'podziel, ten, string, od, początku'

// console.log(text1.toUpperCase());
// console.log(text2.toLowerCase());
// console.log(text3.slice(6));
// console.log(text4.includes('czy'));
// console.log(text5.charAt(2));
// console.log(text6.replaceAll('pies', 'kot'));
// console.log(text7.split(','));



                                                                        /*TYPY DANYCH*/
/* Typy danych - Number*/




// const num1 = 23
// const num2 = '45'

// console.log(num1 + num2);       //konkatenacja czyli łączenie 2 stringów
// console.log(num1 *1);

// const num3 = 'abcd'
// console.log(num3 * 1);         //NaN przykład 

// const num4 = 1232.452
// console.log(num4.toFixed(2));  // Zaokrąglenie po przecinku

// const num5 = '123'
// console.log(parseInt(num5));   // parseInt zamieni nam stringa w numer




/* Typy danych - Boolean*/



/*WARTOŚCI KTÓRE ZAWSZE ZWRÓCĄ NAM FALSE*/
// false
// undefined
// null
// 0
// NaN
// '' - pusty string


// let a = true
// let b = false

// if('') {
//     console.log(true);
// } else {
//     console.log(false);
// }



/* Typy danych - Typy złożone*/



// const colors = ['red', 'green', 'blue']
// console.log(colors);


// function test() {
//     console.log('Cześć, jestem w funkcji!');
// }
// test()


// const person = {
//     name: 'Kuba',
//     age: 31,
//     favColor: 'black'
// }
// console.log(person);


                                                                    /*OPERATORY*/

                                            /*Operatory arytmeyczne*/




// let x = 15
// const y = 11

// const add = x + y
// console.log(x + y);

// const substract = x - y
// console.log(x - y);

// const multiply = x * y
// console.log(x * y);

// const divide = x / y
// console.log(x / y);

// const modulo = x % y
// console.log(x % y);

// // % ++ --

// x++
// x++
// x++
// console.log(x);     /*Inkrementacja podbija wartość o 1*/

// x--
// x--
// x--
// console.log(x);     /*Dekrementacja zmniejsza wartość o 1*/




                                        /*Operatory przypisania*/



// const name = 'Kuba'


// let x = 10
// let y = 15
// console.log(y);


// x = x + y             //x = 10 + 15
// console.log(x);       //x = 25

// x += y                
// console.log(x);       //x = 25 + 15


// x = x - y
// console.log(x);

// x -= y
// console.log(x);


// x = x * y
// console.log(x);

// x *= y
// console.log(x);




                                    /* Operatory Porównania*/


// console.log(10 == '10');    // == Porównuje zawartość
// console.log(10 === '10');   // === Porównuje zawartość oraz typ danych


// console.log(10 != '10');
// console.log(10 !== '10');
// console.log('10' !== 10);   // != Sprawdza czy to co jest po lewej  różni się zawartością od tego co jest po prawej
//                             // != Sprawdza czy to co jest po lewej  różni się zawartością ORAZ typem, danych od tego co jest po prawej
                            

// console.log(10 > 5);
// console.log(10 < 5);
// console.log(10 <= 5);
// console.log(10 >= 5);




                                     /* Operatory logiczne */



// && - i / zwraca TRUE tylko i wyłącznie wtedy, gdy wszystkie warunki są prawdziwe
// || - lub / zwraca False tylko i wyłącznie wtedy, gdy wszystkie warunki są fałszywe
// ! - zaprzeczenie


// if(true && false) {
//     console.log('👍');   
// } else {
//     console.log('👎');
// }




                                                                /* INSTRUKCJE WARUNKOWE */



                                    /* If, else if & else*/


// const pass = 'n!fgbfbgdvjhds76'

// if(pass.length > 10 && pass.includes('!')) {
//     console.log('Masz zajebiste hasło');
// } else if(pass.length > 10) {
//     console.log('Masz dobre hasło');
// } else {
//     console.log('Masz chujowe hasło');
// }

// /*--------------------------------*/

// const color = 'blue'

// if (color == 'blue') {
    
//     if (10 > 0) {
//         console.log('true true');
//     } else {
//         console.log('okokok');
//     }
// } else {
//     console.log('false');
// }

                                        /* Switch */ 

const day = 'środa'

// if (day === 'poniedziałek') {
//     console.log('Dziś jest poniedziałek');
// } else if (day === 'wtorek') {
//     console.log('Dziś jest wtorek');
// } else if (day === 'środa') {
//     console.log('Dziśjest środa');
// } else if (day === 'czwartek') {
//     console.log('Dziś jest czwartek');
// } else if (day === 'piątek') {
//     console.log('Dziśjest piątek');
// } else {
//     console.log('Weekend!');
// }

switch (day) {
    case 'poniedziałek':
        console.log('Dziś jest poniedziałek')
        break
    case 'wtorek':
        console.log('Dziś jest wtorek')
        break
    case 'środa':
        console.log('Dziś jest środa')
        break
    case 'czwartek':
        console.log('Dziś jest czwartek')
        break
    case 'piątek':
        console.log('Dziś jest piątek')
        break

    default:
        console.log('Weekend!');
}



